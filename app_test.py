import logging
import environment
import os

env = os.environ.get('FLASK_ENVIRONMENT') or 'production'

environment.set(env)
from app_main import app

# Bootstrap(app)
# ------------------------------------------------------------------------------------------------
###############         Server Start       ####################################
# ------------------------------------------------------------------------------------------------

if __name__ == '__main__':
    app.logger.setLevel(logging.DEBUG)
    app.run(host='0.0.0.0', port=6543, static_files={'': 'public'}, debug=True)
