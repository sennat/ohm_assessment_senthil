"""task_migration

Revision ID: 49d1b536f0f4
Revises: 00000000
Create Date: 2019-03-18 00:42:47.552576

"""

# revision identifiers, used by Alembic.
revision = '49d1b536f0f4'
down_revision = '00000000'

from alembic import op
import sqlalchemy as sa


def upgrade():
    op.execute('''UPDATE user set point_balance=5000 where user_id=1''')
    op.execute('''INSERT INTO rel_user (user_id, rel_lookup, attribute)
        VALUES
            (2, 'LOCATION', 'USA')
    ''')
    op.execute('''UPDATE user set tier='Silver' where user_id=3''')


def downgrade():
    op.execute('''UPDATE user set point_balance=1 where user_id=1''')
    op.execute('''DELETE from rel_user where user_id=2''')
    op.execute('''UPDATE user set tier='Carbon' where user_id=3''')
